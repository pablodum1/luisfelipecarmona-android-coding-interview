package com.beon.androidchallenge.ui.main

import com.beon.androidchallenge.domain.model.Fact

data class StateClass(
    val isLoading: Boolean = false,
    val fact: Fact? = null,
    val errorMessage: String? = null
)