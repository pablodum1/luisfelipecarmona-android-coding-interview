package com.beon.androidchallenge.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.room.Room
import com.beon.androidchallenge.data.repository.FactRepository
import com.beon.androidchallenge.domain.model.Fact
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

private const val DELAY_AMOUNT = 500L

class MainViewModel : ViewModel() {

    private val _currentFact = MutableLiveData(StateClass())
    val currentFact: LiveData<StateClass> = _currentFact

    private var typeDelay : Job = Job()

    fun searchNumberFact(number: String) {
        typeDelay.cancel()
        typeDelay = viewModelScope.launch {
            delay(DELAY_AMOUNT)
            requestNumberFact(number)
        }
    }

    private fun requestNumberFact(number: String) {
        if (number.isEmpty()) {
            typeDelay.cancel()
            _currentFact.postValue(StateClass(false, null))
            return
        }

        _currentFact.postValue(_currentFact.value?.copy(isLoading = true))
        FactRepository.getInstance().getFactForNumber(
            number,
            object : FactRepository.FactRepositoryCallback<Fact> {
                override fun onResponse(response: Fact) {
                    _currentFact.postValue(StateClass(false, response))
                }

                override fun onError() {
                    _currentFact.postValue(
                        _currentFact.value?.copy(
                            isLoading = false,
                            fact = null,
                            errorMessage = "An error has occurred, please try again later"
                        )
                    )
                }
            })
    }
}

