package com.beon.androidchallenge.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.beon.androidchallenge.domain.model.Fact
import com.beon.androidchallenge.domain.model.FactDao

@Database(entities = [Fact::class], version = 1, exportSchema = false)
abstract class LikedFactDatabase : RoomDatabase() {
    abstract fun factDao(): FactDao
}