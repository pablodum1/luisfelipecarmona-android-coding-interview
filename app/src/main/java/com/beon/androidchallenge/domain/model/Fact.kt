package com.beon.androidchallenge.domain.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class Fact(
    @SerializedName("text")
    var text: String? = null,
    @SerializedName("number")
    var number: Long? = null,
    @SerializedName("found")
    var found: Boolean? = null,
    @SerializedName("type")
    var type: String? = null,
    @PrimaryKey(false)
    var id: Int = 0
) {
}