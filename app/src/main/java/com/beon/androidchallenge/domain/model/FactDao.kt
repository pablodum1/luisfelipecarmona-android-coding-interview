package com.beon.androidchallenge.domain.model

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query

@Dao
interface FactDao {
    @Insert
    suspend fun insert(fact: Fact)

    @Delete
    suspend fun delete(fact: Fact)

    @Query("Select * from Fact")
    suspend fun getAll(): List<Fact>
}